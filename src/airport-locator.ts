import haversine from './haversine'
import { bisector } from 'd3-array'

const { left: bisectDistance } = bisector(({ distance }) => distance)

export interface Airport {
  locationID: string
  siteNumber: string
  type: AirportType
  effectiveDate: string
  region: string
  state: string
  stateName: string
  county: string
  countyState: string
  city: string
  facilityName: string
  latitude: number
  longitude: number
}

enum AirportType {
  AIRPORT,
  SEAPLANE_BASE,
  HELIPORT
}

interface LocatedAirport {
  distance: number
  airport: Airport
}

/**
 * @param {Airport[]} data
 * @returns {number[][]}
 *
 * create N x N array of arrays representing distances between airports
 * entries at a given row or column index correspond to the airport at the same index in data
 *
 * e.g. the value at matrix[i][j] represents the distance from `data[i]` to `data[j]`
 *
 * we don't care about the distance from an airport to itself, so value[i][i] = null
 *
 * distances are symmetric e.g. distance from A to B is same as distance from B to A
 * so matrix[i][j] = matrix[j][i]
 */

const generateDistanceMatrix = (data: Airport[]): number[][] => {
  const size = data.length
  const distanceMatrix: number[][] = [...Array(size)].map(() => new Array(size))

  let idx = -1
  let jdx, distance

  while (++idx < size) {
    distanceMatrix[idx][idx] = null!
    jdx = idx
    while (++jdx < size) {
      distance = +(haversine(data[idx], data[jdx]) / 1e3).toFixed(2)
      distanceMatrix[idx][jdx] = distance
      distanceMatrix[jdx][idx] = distance
    }
  }
  return distanceMatrix
}

/**
 * @param {number[]} row - array of distances
 * @param {number} radius - search radius, in kilometers
 * @param {Airport[]} data - input data
 *
 * sorts distances and filters by search radius.
 * looks up airport record associated with each distance index.
 * @returns {LocatedAirport[]}
 */

function filterAndSort<N, T>(row: N[], radius: N, data: T[]): LocatedAirport[] {
  const size = row.length
  const result: any = []

  let idx = -1
  let distance, insertIdx

  while (++idx < size) {
    distance = row[idx]

    // discard distance greater than specified radius
    if (!distance || distance > radius) continue
    // uses binary search to calculate insertion index that maintains sort order
    insertIdx = bisectDistance(result, distance)
    result.splice(insertIdx, 0, { distance, airport: data[idx] })
  }
  return result
}

/**
 * @param {Airport[]} data - array of Airport data
 * @param {number} radius - search radius, in kilometers
 * @param {number} count - number of airports to return in list
 * @returns {Array<Airport, Airport[]>}
 */

export default function(
  data: Airport[],
  radius: number = 500,
  count: number = 3
): Array<{ airport: Airport; airports: LocatedAirport[] }> {
  const distanceMatrix = generateDistanceMatrix(data)
  return distanceMatrix.map((row, rowIdx) => {
    const airports = filterAndSort(row, radius, data).slice(0, count)

    return {
      airport: data[rowIdx],
      airports
    }
  })
}
