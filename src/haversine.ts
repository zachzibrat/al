const { asin, sin, cos, pow, sqrt, PI } = Math
const toRadians = (degrees: number): number => (degrees * PI) / 180

// haversine angle formula
const hav = (theta: number): number => pow(sin(theta / 2), 2)
export const EARTH_RADIUS = 6378137

interface Location {
  latitude: number
  longitude: number
}

// https://en.wikipedia.org/wiki/Haversine_formula
export default function(
  { latitude: latA, longitude: lngA }: Location,
  { latitude: latB, longitude: lngB }: Location,
  radius = EARTH_RADIUS
): number {
  latA = toRadians(latA)
  latB = toRadians(latB)
  lngA = toRadians(lngA)
  lngB = toRadians(lngB)
  const h = hav(latA - latB) + cos(latA) * cos(latB) * hav(lngA - lngB)
  return 2 * radius * asin(sqrt(h))
}
