import chai from 'chai'
import { describe, it } from 'mocha'
import airportLocator, { Airport } from '../src/airport-locator'

const { expect } = chai

const data = [
  {
    locationID: 'AZ1',
    latitude: 45,
    longitude: 90
  },
  {
    locationID: 'AZ4',
    latitude: 44,
    longitude: 84.5
  },
  {
    locationID: 'AZ2',
    latitude: 46,
    longitude: 88
  },
  {
    locationID: 'AZ5',
    latitude: 45,
    longitude: 42.5
  },
  {
    locationID: 'AZ3',
    latitude: 44,
    longitude: 86.5
  },
  {
    locationID: 'AZ6',
    latitude: 45,
    longitude: 40
  },
  {
    locationID: 'AZ7',
    latitude: 45,
    longitude: 39
  },
  {
    locationID: 'AZ8',
    latitude: 45,
    longitude: 38
  },
  {
    locationID: 'AZ9',
    latitude: 42,
    longitude: 37
  },
  {
    locationID: 'AZ10',
    latitude: 38,
    longitude: 37
  }
] as Airport[]

const IDlookup = id => ({ airport: { locationID } }) => locationID === id

describe('airport locator', () => {
  it('has default settings for search radius (500km) and search count (3 airports)', () => {
    const locator = airportLocator(data)
    const { airports: airportsForAZ1 } = locator.find(IDlookup('AZ1'))!
    const { airports: airportsForAZ9 } = locator.find(IDlookup('AZ9'))!

    const IDsForAZ1 = airportsForAZ1.map(({ airport: { locationID: id } }) => id)
    const IDsForAZ9 = airportsForAZ9.map(({ airport: { locationID: id } }) => id)
    const distances = airportsForAZ1.map(({ distance }) => distance)

    expect(IDsForAZ1).to.deep.equal(['AZ2', 'AZ3', 'AZ4'])
    expect(IDsForAZ9).to.deep.equal(['AZ8', 'AZ7', 'AZ6'])
    expect(Math.max(...distances) <= 500).to.be.true

    // this airport has only one airport within 500km
    expect(locator.find(IDlookup('AZ10'))!.airports.length).to.equal(1)
  })

  it('can set radius and count', () => {
    const locator = airportLocator(data, 4000, 5)
    const { airports: airportsForAZ1 } = locator[0]

    const IDsForAZ1 = airportsForAZ1.map(({ airport: { locationID: id } }) => id)
    const distances = airportsForAZ1.map(({ distance }) => distance)

    expect(IDsForAZ1).to.deep.equal(['AZ2', 'AZ3', 'AZ4', 'AZ5', 'AZ6'])
    expect(Math.max(...distances) <= 4000).to.be.true
  })
})
